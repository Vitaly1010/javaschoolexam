package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.Collections;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null) || inputNumbers.size() > (Integer.MAX_VALUE - 2)) {
            throw new CannotBuildPyramidException();
        }
        int numberInPyramidBase = 0;
        int i = 0;

        do {
            numberInPyramidBase++;
            i += numberInPyramidBase;
        } while (i < inputNumbers.size());

        if (i != inputNumbers.size()) {
            throw new CannotBuildPyramidException();
        }

        Collections.sort(inputNumbers);
        int numberInInputList = 0;
        int[][] resultArray = new int[numberInPyramidBase][2 * numberInPyramidBase - 1];
        int x = 0;
        int y = 0;

        for (int a = numberInPyramidBase + 1; y <= numberInPyramidBase && a >= 0; a--) {
            int b;
            for (b = 0; b < a; b++) {
                x++;
            }

            for (int j = b; j < numberInPyramidBase + 1; j++) {
                x++;
                resultArray[y - 1][x - 2] = inputNumbers.get(numberInInputList);
                x++;
                numberInInputList++;
            }

            x = 0;
            y++;
        }
        return resultArray;
    }
}

