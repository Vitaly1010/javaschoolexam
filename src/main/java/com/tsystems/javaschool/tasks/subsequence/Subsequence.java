package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        int index = 0;
        if (x.size() == 0) {
            return true;
        }

        for (Object o : y) {
            if (index == x.size()) {
                return true;
            }
            if (o.equals(x.get(index))) {
                index++;
            }
        }
        return false;
    }
}
