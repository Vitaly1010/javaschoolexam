package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.NoSuchElementException;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.isEmpty()) {
            return null;
        }
        LinkedList<Double> st = new LinkedList<>();
        LinkedList<Character> op = new LinkedList<>();

        for (int i = 0; i < statement.length(); i++) {
            char c = statement.charAt(i);
            if (isDelim(c)) {
                continue;
            }
            if (c == '(') {
                op.add('(');
            } else {
                if (c == ')') {
                    try {
                        while (op.getLast() != '(') {
                            processOperator(st, op.removeLast());
                        }
                        op.removeLast();
                    } catch (NoSuchElementException e) {
                        return null;
                    }
                } else {
                    if (isOperator(c)) {
                        try {
                            while (!op.isEmpty() && priority(op.getLast()) >= priority(c)) {
                                processOperator(st, op.removeLast());
                            }
                            op.add(c);
                        } catch (NoSuchElementException e) {
                            return null;
                        }
                    } else {
                        StringBuilder operand = new StringBuilder();
                        while (i < statement.length() && (Character.isDigit(statement.charAt(i)) || statement.charAt(i) == '.')) {
                            operand.append(statement.charAt(i++));
                        }
                        --i;
                        try {
                            st.add(Double.parseDouble(operand.toString()));
                        } catch (NumberFormatException e) {
                            return null;
                        }
                    }
                }
            }
        }
        try {
            while (!op.isEmpty()) {
                processOperator(st, op.removeLast());
            }
        } catch (NoSuchElementException e) {
            return null;
        }
        DecimalFormat df = new DecimalFormat("##.####");
        String result = df.format(st.get(0)).replace(",", ".");

        try {
            Double.parseDouble(result);
        } catch (NumberFormatException e) {
            return null;
        }
        return result;
    }

    static boolean isOperator(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/';
    }

    static boolean isDelim(char c) {
        return c == ' ';
    }

    static int priority(char operator) {
        switch (operator) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            default:
                return -1;
        }
    }

    static void processOperator(LinkedList<Double> st, char op) {
        double firstNumber = st.removeLast();
        double secondNumber = st.removeLast();
        switch (op) {
            case '+':
                st.add(secondNumber + firstNumber);
                break;
            case '-':
                st.add(secondNumber - firstNumber);
                break;
            case '*':
                st.add(secondNumber * firstNumber);
                break;
            case '/':
                st.add(secondNumber / firstNumber);
                break;
        }
    }
}
